import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FizzBuzzTest {
    @Test
    public void divisibleBy3() {
        String expected = "Fizz";
        String actual = new FizzBuzz(3).divisibleBy3or5();

        assertEquals(expected, actual, "Returns 'Fizz' when divisible by 3");
    }

    @Test
    public void notDivisibleBy3() {
        String expected = "4";
        String actual = new FizzBuzz(4).divisibleBy3or5();

        assertEquals(expected, actual, "Returns \"4\"");
    }

    @Test
    public void divisibleBy3and5() {
        String expected = "FizzBuzz";
        String actual = new FizzBuzz(15).divisibleBy3or5();

        assertEquals(expected, actual, "Returns 'FizzBuzz' when divisible by 3 and 5");
    }

    @Test
    public void divisibleBy5() {
        String expected = "Buzz";
        String actual = new FizzBuzz(10).divisibleBy3or5();

        assertEquals(expected, actual, "Returns 'FizzBuzz' when divisible by 5");
    }
}
