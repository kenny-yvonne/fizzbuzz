public class FizzBuzz {
    private Integer num;

    public FizzBuzz(Integer num) {
        this.num = num;
    }

    public String divisibleBy3or5() {
        if (num % 15 == 0) {
            return "FizzBuzz";
        } else if (num % 3 == 0) {
            return "Fizz";
        } else if (num % 5 == 0) {
            return "Buzz";
        } else {
            return num.toString();
        }
    }
}
